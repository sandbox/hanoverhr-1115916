<?php

//This module will allow a user to enter a group (or group token) and then specify upon an event, which roles should be contacted.
//Sean R. Hanover, HHC, Inc, www.hhc-inc.com. 21 Mar 2011.  Hats off to the rest of the DRT consulting team including Chris Zeiders, Nneka Hector, Josh Eagle, Patrick Harding and Chris Klann.
//Credit also to http://dominiquedecooman.com/blog/drupal-custom-rules-how-write-your-own-events-conditions-actions-and-custom-object-custom-token
//I  learned how to manipulate rules and tokens from the above link.  Highly recommended!

//Define the rule we want to implement.  MODULE is the the sub heading in the drop down Rule menu we want this action to appear under.  
//EVAL INPUT is the number of fields,and their names, that the rule is looking for when it runs.
function OGEmail_rules_action_info ()
{
	return array(
	   'OGEmail_action_mail_to_users_of_group'=>array (
		'label'=>t('Send an email to specific roles inside a group.'),
		'module'=>'OGEmail',
		'eval input'=>array('subject','message','from','group','role'),
	   ),
	);
}

//Display the settings form to get user to indicate what group, etc.
//The number and type of input should correspond to EVAL INPUT from the action_info function.
function OGEmail_action_mail_to_users_of_group_form($settings = array(), &$form) 
{
  $form['settings']['group'] = array(
    '#type' => 'textfield',
    '#size' => 10,
    '#title' => t('Group ID'),
    '#prefix' => t('To determine your Group ID {Node}, visit the office in question and read the url. Or, use the token [node:groups] to select ALL groups associated with this node.  [node:groups] was created in OGEmail.module.'),
    '#required' => TRUE,
    '#default_value' => isset($settings['group']) ? $settings['group'] : array('[node:groups]'),
    '#description' => t('Supply the Group ID (nid) of the target group.'),
  );


//Load the different roles, and display them for the user.
$UR=user_roles();
ksort($UR);  // This will sort by the key, ensuring that Role ID's are the sort order of the day.

//This is wrapper.  Should really go in a CSS file, but only limited CSS, so probably safer to just include it here.
$Roles='Possible role selection:<BR>';
$Roles=$Roles.'<table>';
$Roles=$Roles.'<tr><td style="background-color: #E9EFF2;width:10px;">Role ID</td><td style=" background-color: #E9EFF2;width:200px;">Definition</td></tr>';
$Roles=$Roles.'<tr>';
foreach ($UR as $key=>$WhichRole)
{
	$Roles=$Roles.'<tr><td border="1">'.$key.'</td><td>'.$WhichRole.'</td></tr>';
}
$Roles=$Roles.'</table>';

  $form['settings']['role']=array(
    '#type' => 'textfield',
    '#size'=>3,
    '#title'=>t("Group Role [Role ID]"),
    '#prefix'=>$Roles,
    '#required' => TRUE,
    '#default_value' => isset($settings['role']) ? $settings['role'] : array(),
    '#description'=>t('Provide which role within this group should be notified of the state change.  One role per action rule.  
			Use multiple rules to control multiple roles.  NOTE:  If a node of type: "og_email_configuration" and title: "OG Email Configuration"
			exists, and within the body of that node are email addresses seperated by commas *must be commas*, email will NOT be
			sent to the roles for this group, but rather to the email addresses in that node.'),
  );



  // As per standard drill, get the rest of the input from the mail form.
  rules_action_mail_to_user_form($settings, $form);
}


//Implement the rule when triggered
function OGEmail_action_mail_to_users_of_group($settings) 
{

	//Make sure the from won't cause errors.  We just strip returns here.  We might want to check for invalid characters, too.
  	$from = ($settings['from']) ? str_replace(array("\r", "\n"), '', $settings['from']) : 'web_support@yoursite.gov';

	if ($group = $settings['group']) 
 	{

		//RID = Role ID = default (3)
		//GID = Node id of the group
		$rid= isset($settings['role']) ? $settings['role'] : 3;
		$gid = $group;


		//We need to account for multiple groups, if appropriate.  We get this from either multiple inputs on the form or [node:groups]
		//For the token "[node:groups]" see OGEmail.module. SRH 22 Mar 2011
		$WhichGroup=explode(",",$group);
		foreach ($WhichGroup as $key=>$GroupID)
		{
			if ($GroupID<>"")
			{
				if ($key==0)
				{
					$Suffix=$Suffix.' AND ((og_uid.nid = '.$GroupID.')';
				}else{
					$Suffix=$Suffix.' OR (og_uid.nid = '.$GroupID.')';
				}
			}
		}			

		$Suffix=$Suffix.')';  //We need to surroung the "AND" section of groupid's in its own brackets, so all AND matches are evaluated as a group, not as seperate AND/OR statements..


		//Also need to make sure we fix a Drupal glitch.  Apparently, user in "pending acceptance" status to group membership, are treated as belonging to the group
		//and will get emails.  Must manually prevent this.

		//select is_active from og_uid where nid=[og nid] and uid=[userid];  WHERE is_active=1 for active, 0=for pending and null for not associated with the group at all.

		$Query = "SELECT DISTINCT * FROM users
				INNER JOIN users_roles ON users.uid = users_roles.uid
				LEFT JOIN og_uid ON users.uid = og_uid.uid
		 	 WHERE (users_roles.rid =".$rid.") AND og_uid.is_active=1 AND users.status<>0 ".$Suffix;
	
	  // We are NOT escaping or protecting against $SUFFIX because by definition, it DOES contain SQL code we need to execute.
		$Result = db_query($Query);

	


		//Check flag to be included in the form of a NODE.  If this node has content, then send only to the people in the body of the node.  Otherwise, send to the group members.
		$Query3='select node_revisions.body
				from node, node_revisions
				where node.type="og_email_configuration" and node.title="OG Email Configuration" 
				and node.nid=node_revisions.nid and node.vid=node_revisions.vid;';
		$Result3=db_query($Query3);
		$NameList[1]=db_fetch_array($Result3);
		$NameList[1]['body']=strip_tags($NameList[1]['body']);
		$Temp=$NameList[1]['body'];
		$EmailList=explode(',',$Temp);

		if (count($EmailList)>0)
		{	//If the node has a list of people, send to THAT list and NOT the group role members.

			foreach ($EmailList as $SendTo)
			{
				$message = drupal_mail('OGEmail', 'action_mail_to_users_of_group', $SendTo, language_default(), $settings, $from);
    
	    			if ($message['result']) 
    				{
					//Post notice in watchdog that the email has been sent successfully.  We can comment this out if tracking is not important.
      					watchdog('OGEmail NODE MODE', 'Successfully sent email to '.$member->mail.' of GID:'.$group.' Role: '.$rid);
    				}
			} // End checking for a live member.  We don't want to send to inactive members.
		}else{


	    		// Send the messages to each member with the approved role.
    			$message = array('result' => TRUE);
    			while ($member = db_fetch_object($Result)) 
	    		{
				if ($member->status<>0)
				{
    					$message = drupal_mail('OGEmail', 'action_mail_to_users_of_group', $member->mail, language_default(), $settings, $from);
    	
		    			if ($message['result']) 
	    				{
						//Post notice in watchdog that the email has been sent successfully.  We can comment this out if tracking is not important.
      						watchdog('rules - OG Email', 'Successfully sent email to '.$member->mail.' of GID:'.$group.' Role: '.$rid);
    					}
				} // End checking for a live member.  We don't want to send to inactive members.
	    		}//Loop through each member

		}// End testing if this is a practice node send or the real thing.

  	} // Endif GROUP<>null
}//End function ACTION


//Clean up body of mail message
function OGEmail_mail($key, &$message, $settings)
{
	$message['subject']=str_replace(array('\r','\n'),"",$settings['subject']);
	$message['body'][]=drupal_html_to_text($settings['message']);
}




?>