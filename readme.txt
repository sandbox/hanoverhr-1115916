This module will only be helpful if Rules are installed.  While not strictly dependent on Rules (and hence, no dependencies) to execute properly, the module won't do anything without Rules, as it modifies/augments a rules function.

-----

To install, create a directory called OG_Email in the sites/all/modules directory.  Therein, place the files OGEmail.module, OGEmail.info, and OGEmail.rules.inc.  

Within Drupal, navigate to the modules page in the administrative section of your site (...admin/build/modules/list).  Click on the box by OG Email Rules to turn on the module.  Save/Exit the modules screen.

Your module is installed!  There is nothing else to configure -- however, if you wish to provide an alternative email address in your notifications (not required), change line 74 in the OGEmail.rules.inc file to reflect the correct email).

To use the functionality created by the module, create a new rule, and select OG Email as an action.  Follow the directions on the action configuration page (of the action itself, within Rules) for creating a specialized node that by-passes OG Roles/Email addresses.

Questions?  Feel free to post them on the Drupal.org page for this project, or contact me directly at SeanHanover@hhc-inc.com.  I welcome feedback and comments!
